const Loader = () => 
  <>
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-content">
        <span className="icon is-large">
          <i className="fas fa-spinner fa-pulse fa-3x"></i>
        </span>
      </div>
    </div>
    <style jsx>{`
      .modal-content {
        text-align: center;
      }
      .icon {
        color: white;
      }
    `}</style>
  </>

export default Loader;