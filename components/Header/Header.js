import Logo from '../Logo/Logo';

const Header = () => (
  <>
  <div className="navbar is-fixed-top is-black">
    <div className="navbar-brand">
      <span className="navbar-item">
        <Logo/>
      </span>
    </div>
  </div>
  <style jsx>{`
    .navbar-brand {
      justify-content: center;
      width: 100%;
    }
  `}</style>
  </>
)

export default Header;