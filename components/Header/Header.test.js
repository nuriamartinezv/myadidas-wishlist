import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';
import Logo from '../Logo/Logo';

describe('Header', () => {
  it('should contain adidas logo', () => {
    const component = shallow(<Header/>);
    const logo = component.contains(<Logo/>);
    expect(logo).toBeTruthy();
  })
})