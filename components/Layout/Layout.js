import Header from '../Header/Header';
import NavBar from '../NavBar/NavBar';

const Layout = (props) => (
  <div className="section">
    <div className="container">
      <Header/>
      <NavBar/>
      {props.children}
    </div>
  </div>
);

export default Layout;