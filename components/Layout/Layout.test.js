import React from 'react';
import { shallow } from 'enzyme';
import Layout from './Layout';
import Header from '../Header/Header';
import NavBar from '../NavBar/NavBar';


describe('Layout', () => {
  const component = shallow(<Layout/>);

  it('should contain Header', () => {
    const isHeader = component.contains(<Header/>);
    expect(isHeader).toBeTruthy();
  })

  it('should contain NavBar', () => {
    const isNavBar = component.contains(<NavBar/>);
    expect(isNavBar).toBeTruthy();
  })

})