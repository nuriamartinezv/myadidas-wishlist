import React from 'react';
import { mount } from 'enzyme';
import ConnectedNavBar from './NavBar';
import configureStore from 'redux-mock-store';


describe('NavBar', () => {
  const state = {
    wishlist: []
  };

  const mockStore = configureStore();
  let store, component;

  beforeEach(()=>{
    store = mockStore(state);
    component = mount(<ConnectedNavBar store={store}/>);
  });
  

  it('should contain Link to /', () => {
    const reference = component.find('Link').at(0).props('href').href;
    expect(reference).toBe('/');
  });

  it('should contain Link to /wishlist', () => {
    const reference = component.find('Link').at(1).props('href').href;
    expect(reference).toBe('/wishlist');
  });
  
  it('should show the number of items in wishlist', () => {
    const counter = component.find('.number-of-items').text();
    expect(counter).toBe('0');
  });

})