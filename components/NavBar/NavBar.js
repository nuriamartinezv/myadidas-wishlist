import Link from 'next/link';
import { connect } from 'react-redux';

const NavBar = (props) => {
  const LinksData = [
    {
      pathname: '/',
      text: 'Search'
    },
    {
      pathname: '/wishlist',
      text: 'Wishlist',
      size: props.wishlist.length,
      icon: 'heart'
    }
  ];
  
  return (
    <div>
      <ul className="level is-mobile">
        {LinksData.map((link, index) => (
          <li className="level-item" key={index}>
            <Link href={link.pathname}>
              <a className={`button ${props.pathname === link.pathname ? "is-light" : "is-white"}`}>
                {link.text}
                {(link.size !== undefined) ? 
                  <span className="tag is-warning">
                    <span className="icon"><i className={`fas fa-${link.icon}`}></i></span>
                    <span className="number-of-items">{link.size}</span>
                  </span> :
                  <></>
                }
              </a>
            </Link>
          </li>
        ))}
      </ul>
      <style jsx>{`
        ul {
          border-bottom: 1px solid #ddd;
          padding-bottom: 45px;
          margin-bottom: 60px;
        }
        li {
          text-align: center;
        }
        .button {
          text-transform: uppercase;
        }
        .tag {
          margin-left: 8px;
        }
      `}</style>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    wishlist: state.wishlist,
    pathname: state.pathname
  };
};

export default connect(
  mapStateToProps,
)(NavBar);