import React from 'react';
import { mount } from 'enzyme';
import ConnectedProduct from './Product';
import configureStore from 'redux-mock-store';


describe('Product', () => {
  const product1 = {
    title: "I'm in wishlist",
    imgSrc: "in.jpg",
    id: "1"
  }
  const product2 = {
    title: "I'm not in wishlist",
    imgSrc: "out.jpg",
    id: "2"
  }
  const state = {
    wishlist: [product1]
  };

  const mockStore = configureStore();
  let store, component;

  beforeEach(()=>{
    store = mockStore(state);
  })

  it('should contain Button that shows the product is in wishlist', () => {
    component = mount(<ConnectedProduct data={product1} store={store} />);
    const inWishlist = component.find('button').hasClass('inWishlist');
    expect(inWishlist).toBe(true);
  })

  it('should contain Button that shows the product is out of wishlist', () => {
    component = mount(<ConnectedProduct data={product2} store={store} />);
    const outWishlist = component.find('button').hasClass('outWishlist');
    expect(outWishlist).toBe(true);
  })
})