import { actionTypes } from '../../store';
import { connect } from 'react-redux';

import { postProduct } from './utils/postProduct';
import { deleteProduct } from './utils/deleteProduct';

const Product = (props) => (
  <div className="box">
    <div className="image">
      <img 
        src={props.data.imgSrc} 
        alt={props.data.title} 
        height="100" 
        width="100"/>
      {(props.wishlist.some(product => product['id'] === props.data.id)) ?
        <button 
          className="inWishlist button is-text"
          onClick={() => deleteProduct(props)}>
            <span className="icon is-medium">
              <i className="fas fa-lg fa-heart"></i>
            </span>
          </button> : 
        <button
          className="outWishlist button is-text"
          onClick={() => postProduct(props)}>
            <span className="icon is-medium">
              <i className="far fa-lg fa-heart"></i>
            </span>
          </button>
      }
    </div>
    <h3>{props.data.title}</h3>
    <p>{props.data.subtitle}</p>

    <style jsx>{`
      .box {
        height: 100%;
        text-align: center;
      }
      .image {
        position: relative;
        margin: 0 auto 15px;
        max-width: 120px;
      }
      .button {
        position: absolute;
        top: 2px;
        right: 2px;
        text-decoration: none;
      }
      h3 {
        max-width: 180px;
        margin: auto;
      }
      p {
        font-size: 12px;
        font-weight: bold;
      }
    `}</style>
  </div>
)


const mapStateToProps = state => {
  return {
    wishlist: state.wishlist
  }
};

const mapDispatchToProps = dispatch => {
  return {
    addProduct: (product) => dispatch(
      {
        type: actionTypes.ADD_PRODUCT_WISHLIST,
        payload: product
      }
    ),
    removeProduct: (product) => dispatch(
      {
        type: actionTypes.REMOVE_PRODUCT_WISHLIST,
        payload: product
      }
    ),
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product);