export const deleteProduct = (props) => {
  const url = `http://localhost:5000/api/wishlist/${props.data.id}`;
  const opt = {
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json'
    }
  }
  fetch(url, opt)
    .then(res => res.json())
    .then(res => {
      if(res.deleted) {
        props.removeProduct(props.data)
      }
    })
};