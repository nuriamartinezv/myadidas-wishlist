export const postProduct = (props) => {
  const url = 'http://localhost:5000/api/wishlist'
  const opt = {
    method: 'POST',
    headers:{
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(props.data)
  }
  fetch(url, opt)
    .then(res => res.json())
    .then(res => {
      if(res.saved) {
        props.addProduct(props.data)
      }
    })
};