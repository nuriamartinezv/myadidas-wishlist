import Product from '../Product/Product';

const ProductList = (props) => (
  <div>
    <ul className="columns is-multiline is-mobile">{props.productList.map((product, key) => 
      <li className="column is-one-quarter-tablet is-half-mobile" key={key}><Product data={product}/></li>)}
    </ul> 
  </div>
)

export default ProductList;