import React from 'react';
import { shallow } from 'enzyme';
import ConnectedSearch from './Search';
import configureStore from 'redux-mock-store';


describe('Search', () => {

  const state = {
    searchInput: ""
  };
  const mockStore = configureStore();
  let store, component;

  beforeEach(()=>{
    store = mockStore(state);
    component = shallow(<ConnectedSearch store={store} />);
  })

  it('check input matches with state input', () => {
    const searchInput = component.props().children.props.searchInput;
    expect(searchInput).toEqual(state.searchInput)
  });

})