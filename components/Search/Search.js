import { actionTypes } from '../../store';
import { connect } from 'react-redux';

import { dispatchSearchProducts } from './utils/dispatchSearchProducts';

const Search = (props) => (
  <div className="level">
    <input 
      className="input" 
      type="text" 
      placeholder="Search"
      value={props.searchInput}
      onChange={() => props.getSearchInput()} 
      onKeyDown={onKeyPress.bind(this, props)}/>
    <button
      className="button is-dark" 
      onClick={() => props.searchProducts()}>
        <span className="icon">
          <i className="fas fa-search"></i>
        </span>
    </button>
    <style jsx>{`
    .level {
      margin-top: -60px;
    }
    .button {
      position: absolute;
      right: 0;
      padding-left: 20px;
      padding-right: 20px;
    }
  `}</style>
  </div>
)

const onKeyPress = (props, e) => {
  if (e.keyCode === 13) {
    props.searchProducts();
  }  
}

const mapStateToProps = state => {
  return {
    searchInput: state.searchInput
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getSearchInput: () => dispatch(
      { type: actionTypes.GET_SEARCH_INPUT }
    ),
    searchProducts: () => dispatch(dispatchSearchProducts()),
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);