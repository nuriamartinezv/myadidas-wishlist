import { actionTypes } from '../../../store';

export const dispatchSearchProducts = () => {
  class Product {
    constructor(title, subtitle, imgSrc, id) {
      this.title = title;
      this.subtitle = subtitle;
      this.imgSrc = imgSrc;
      this.id = id.replace(/h.*\/(.*).html/g, "$1")
    }
  }
  return (dispatch, getState) => {
    const state = getState();
    const searchInput = state.searchInput;
    dispatch({
      type: actionTypes.SET_LOADING,
      payload: true
    })
    setTimeout(() => {
      fetch('https://www.adidas.co.uk/api/suggestions/' + searchInput)
      .then(res => res.json())
      .then(res => {
        let searchProducts = res.products;
        let foundProducts = [];
        searchProducts.map(product => {
          foundProducts.push(
            new Product(
              product.suggestion, product.subTitle, product.image, product.url
            )
          );
        })
        dispatch({
          type: actionTypes.GET_SEARCH_PRODUCTS,
          payload: foundProducts
        })
        dispatch({
          type: actionTypes.SET_LOADING,
          payload: false
        })
      }).catch(() => {
        dispatch({
          type: actionTypes.GET_SEARCH_PRODUCTS,
          payload: []
        })
        dispatch({
          type: actionTypes.SET_LOADING,
          payload: false
        })
      });
    }, 500);
  }
};