FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

# Expose backend API
EXPOSE 5000

# Expose frontend app
EXPOSE 3000

CMD [ "npm", "start" ]
