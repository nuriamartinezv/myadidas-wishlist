import fetch from 'isomorphic-unfetch';
import { actionTypes } from '../store';
import { connect } from 'react-redux';

import Loader from '../components/Loader/Loader';
import Layout from '../components/Layout/Layout';
import Search from '../components/Search/Search';
import ProductList from '../components/ProductList/ProductList';

const Index = (props) => (
  <div>
    {props.loading ? <Loader /> : ""}
    <Layout>
      <Search/>
      {(props.searchProducts !== null) ?
        <ProductList productList={props.searchProducts}/> :
        <p>There were no results found. Make sure the spelling is correct or try another word.</p>
      }
    </Layout>
    <style jsx>{`
      p {
        color: #aaa;
      }
    `}</style>
  </div>
)

Index.getInitialProps = async ({store, pathname}) => {
  const url = 'http://localhost:5000/api/wishlist';
  console.log(pathname)
  await fetch(url)
    .then(res => res.json())
    .then(res => {
      store.dispatch({type: actionTypes.GET_PRODUCT_WISHLIST, payload: res});
      store.dispatch({type: actionTypes.SET_PATHNAME, payload: pathname});
    })
  return {}
};

const mapStateToProps = state => {
  return {
    searchProducts: state.searchProducts,
    loading: state.loading
  };
};

export default connect(
  mapStateToProps,
)(Index);