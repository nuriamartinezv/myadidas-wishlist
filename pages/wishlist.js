import fetch from 'isomorphic-unfetch';
import { actionTypes } from '../store';
import { connect } from 'react-redux';

import Layout from '../components/Layout/Layout';
import ProductList from '../components/ProductList/ProductList';

const Wishlist = (props) => (
  <div>
    <Layout>
      {(props.wishlist.length > 0) ?
        <ProductList productList={props.wishlist}/> :
        <p>Your wishlist is empty</p>
      }
    </Layout>
    <style jsx>{`
      p {
        font-size: 24px;
        color: #aaa;
        text-align: center;
      }
    `}</style>
  </div>
)

Wishlist.getInitialProps = async ({store, pathname}) => {
  const url = 'http://localhost:5000/api/wishlist';
  console.log(pathname)
  await fetch(url)
    .then(res => res.json())
    .then(res => {
      store.dispatch({type: actionTypes.GET_PRODUCT_WISHLIST, payload: res});
      store.dispatch({type: actionTypes.SET_PATHNAME, payload: pathname});
    })
  return {}
};

const mapStateToProps = state => {
  return {
    wishlist: state.wishlist
  }
};

export default connect(
  mapStateToProps,
)(Wishlist);;