import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

const initialState = {
  searchInput: "",
  searchProducts: [],
  wishlist: [],
  pathname: "",
  loading: false
};

export const actionTypes = {
  GET_SEARCH_INPUT: "GET_SEARCH_INPUT",
  GET_SEARCH_PRODUCTS: "GET_SEARCH_PRODUCTS",
  ADD_PRODUCT_WISHLIST: "ADD_PRODUCT_WISHLIST",
  REMOVE_PRODUCT_WISHLIST: "REMOVE_PRODUCT_WISHLIST",
  GET_PRODUCT_WISHLIST: "GET_PRODUCT_WISHLIST",
  SET_PATHNAME: "SET_PATHNAME",
  SET_LOADING: "SET_LOADING"
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.GET_SEARCH_INPUT:
      let searchInput = event.target.value;
      return { ...state, searchInput: searchInput }
    
    case actionTypes.GET_SEARCH_PRODUCTS:
      if (action.payload.length === 0) {
        return { ...state, searchProducts: null }
      } else {
        return { ...state, searchProducts: action.payload }
      }

    case actionTypes.ADD_PRODUCT_WISHLIST:
      let wishlistAddHandler = [...state.wishlist];
      wishlistAddHandler.push(action.payload);
      return { ...state, wishlist: wishlistAddHandler }

    case actionTypes.REMOVE_PRODUCT_WISHLIST:
      let wishlistRemoveHandler = [...state.wishlist]
      let productPosition = wishlistRemoveHandler.indexOf(action.payload);
      wishlistRemoveHandler.splice(productPosition, 1);
      return { ...state, wishlist: wishlistRemoveHandler }

    case actionTypes.GET_PRODUCT_WISHLIST:
      return { ...state, wishlist: action.payload }

    case actionTypes.SET_PATHNAME:
      return { ...state, pathname: action.payload }

    case actionTypes.SET_LOADING:
        return { ...state, loading: action.payload }

    default:
      return state;
  } 
}

const makeStore = (init) => {
  return createStore(reducer, init, applyMiddleware(thunk));
}

export default makeStore;