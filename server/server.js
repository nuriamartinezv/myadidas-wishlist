const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 5000;
const endpoint = '/api/wishlist';

const deleteProduct = require('./utils/deleteProduct');
const addProduct = require('./utils/addProduct');
const searchProduct = require('./utils/searchProduct');

app.use(bodyParser.json());
app.use(cors());

app.listen(port, () => console.log(`API:  http://localhost:${port}${endpoint}`));

let wishlist = []

app.get(`${endpoint}`, (req, res) => {
  res.send(wishlist);
});

app.get(`${endpoint}/:id`, (req, res) => {
  let productId = req.params.id;
  let product = searchProduct(productId, wishlist);
  res.send(product);
});

app.post(`${endpoint}`, (req, res) => {
  let productToAdd = req.body;
  wishlist = addProduct(productToAdd, wishlist);
  res.send({ saved: true });
});

app.delete(`${endpoint}/:id`, (req, res) => {
  let productId = req.params.id;
  wishlist = deleteProduct(productId, wishlist);
  res.send({ deleted: true });
});