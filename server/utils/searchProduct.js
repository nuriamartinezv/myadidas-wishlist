const searchProduct = (productId, productList) => {
  let product;
  productList.forEach((item) =>{
    if (productId === item.id) {
      product = item;
    }
  })
  return product;
}

module.exports = searchProduct;