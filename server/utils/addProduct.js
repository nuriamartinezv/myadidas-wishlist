const addProduct = (product, productList) => {
  productList.push(product);
  return productList;
}

module.exports = addProduct;