const deleteProduct = (productId, productList) => {
  productList.map((item, index) =>{
    if (productId === item.id) {
      productList.splice(index, 1);
    }
  })
  return productList;
}

module.exports = deleteProduct;