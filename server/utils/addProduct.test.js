import addProduct from './addProduct';

describe('addProducts', () => {
  it('should add an item to a list', () => {
    const itemToAdd = {title: 'hello1', id:"1"};
    const itemToRemain1 = {title: 'hello2', id:"2"};
    const itemToRemain2 = {title: 'hello3', id:"3"};
    const itemList = [itemToRemain1, itemToRemain2];
    const newItemList = [itemToRemain1, itemToRemain2, itemToAdd];
    expect(addProduct(itemToAdd, itemList)).toMatchObject(newItemList);
  })
})