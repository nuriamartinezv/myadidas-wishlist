import searchProduct from './searchProduct';

describe('searcProducts', () => {
  it('should search an item from a list', () => {
    const itemToSearch = {title: 'hello1', id:"1"};
    const itemToRemain1 = {title: 'hello2', id:"2"};
    const itemToRemain2 = {title: 'hello3', id:"3"};
    const itemList = [itemToRemain1, itemToSearch, itemToRemain2];
    expect(searchProduct(itemToSearch.id, itemList)).toMatchObject(itemToSearch);
  })
})