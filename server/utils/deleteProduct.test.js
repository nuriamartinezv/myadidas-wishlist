import deleteProduct from './deleteProduct';

describe('deleteProducts', () => {
  it('should delete an item from a list', () => {
    const itemToDelete = {title: 'hello1', id:"1"};
    const itemToRemain1 = {title: 'hello2', id:"2"};
    const itemToRemain2 = {title: 'hello3', id:"3"};
    const itemList = [itemToRemain1, itemToDelete, itemToRemain2];
    const newItemList = [itemToRemain1, itemToRemain2];
    expect(deleteProduct(itemToDelete.id, itemList)).toMatchObject(newItemList);
  })
})